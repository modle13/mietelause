import os
import sys
import json

with open('export.json') as file:
    contents = file.read()

data = list()
data = json.loads(contents)

for entry in data:
    for ism in entry['isms']:
        if isinstance(ism['tags'], str):
            new_tags = [ism['tags']]
            ism['tags'] = new_tags

with open('export.json', 'w') as file:
    file.write(json.dumps(data))
