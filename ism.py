#!/usr/bin/python3

import argparse
from data import sources
import sys

parser = argparse.ArgumentParser()
parser.add_argument('--foo', help='the foo')
parser.add_argument('--add', choices=['source', 'ism'], help='what to add')
args = parser.parse_args()
print(f'args are {args.add}')


def main():

    if args.add:
        print(f'got an add: {args.add}')

    if args.add == 'ism':
        print('adding ism')

    if args.add == 'source':
        print('adding source')
        author = input('author : ')
        title = input('title : ')
        check_source(title, author)
        add_source(title, author)


def check_source(title, author):
    for entry in sources:
        print(entry.isms)
        if entry.title == title and entry.author == author:
            print(f'cannot add duplicates: {title}, {author}')
            sys.exit(1)
 
def add_source(title, author):
    print(f'adding source {title} {author}')
    

main()
