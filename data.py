import os
import sys
import json
import operator

data = list()
tags = list()
unique_tags = list()
total_count = 0
sources = list()


def main():
    global data
    with open('export.json') as file:
        contents = file.read()

    data = json.loads(contents)

    for entry in data:
        sources.append(Source(entry))

    tags, unique_tags = get_tags()
    total_count = get_ism_count(data)


class Ism(object):
    number = -1
    quote = None
    comments = None
    _id = None
    tags = []

    def __init__(self, dictionary):
        self.__dict__.update(dictionary)


class Source(object):
    _id = None
    added = -1
    title = None
    author = None
    isms = []

    def __init__(self, dictionary):
        self.__dict__.update(dictionary)


def get_ism_count(data):
    count = 0
    temp_isms = list()
    for item in data:
        temp_isms.extend(item['isms'])
    return len(temp_isms)


def filter_on_tag(tag):
    matches = list()
    # .copy() so we don't clobber data
    for item in data.copy():
        # .copy() so we don't clobber items
        new_item = item.copy()
        # list comp here to remove isms that don't have tag
        # .copy() not needed here since we're updating the new item and not changing tags
        # if tags were changing, copy would be needed
        new_item['isms'] = [ism for ism in new_item['isms'] if tag in ism['tags']]
        matches.append(new_item)
    return matches


def get_tags():
    temp_tags = list()
    tag_set = set()
    counted_tags = dict()

    for item in data:
        for ism in item['isms']:
            # gets all tag occurrences, with dups
            temp_tags.extend(ism['tags'])
            # removes dups
            tag_set = set(temp_tags) ^ set(ism['tags'])

    # counts all tags into a dict
    counted_tags = {x:temp_tags.count(x) for x in temp_tags if x}
    counted_tags = sorted(counted_tags.items(), key=lambda kv: kv[1], reverse=True)
    # removes blanks
    tag_set = [i for i in tag_set if i]
    return counted_tags, sorted(tag_set)


#if __name__ == '__main__':
main()
