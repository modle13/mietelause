# usage

```
pipenv --python 3.7
pipenv install
pipenv run gunicorn -b0.0.0.0:8000 wsgi:app
```

## Openshift deploy

```
oc new-app https://gitlab.com/modle13/mietelause.git
oc expose svc/mietelause
```
