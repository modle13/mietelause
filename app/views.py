from flask import render_template
from app import app

from data import data, get_ism_count, filter_on_tag, tags, total_count, unique_tags

import json


@app.route('/')
def main():
    return render_template('index.html', data=data, count=total_count)

@app.route('/filter/<filter_type>/<value>')
def filter(filter_type, value):
    if filter_type in ['title', 'author']:
        filtered_data = [x for x in data if x[filter_type] == value]
    elif filter_type == 'tag':
        filtered_data = filter_on_tag(value)
    return render_template('index.html', filter=(filter_type, value), data=filtered_data, count=get_ism_count(filtered_data))

@app.route('/tags')
def get_tags():
    return render_template('tags.html', data=unique_tags)

@app.route('/tag_counts')
def get_counted_tags():
    return render_template('tags.html', data=tags)

@app.route('/sources')
def get_sources():
    return render_template('sources.html', data=data)
